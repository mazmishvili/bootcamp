$(document).ready(function(){
    $('.next').click(function(){
        $('.pagination').find('.circle.active').next().addClass('active');
        $('.pagination').find('.circle.active').prev().removeClass('active');
    })

    $('.next2').click(function(){
        $('.pagination').find('.circle.active').next().addClass('active');
        $('.pagination').find('.circle.active').prev().removeClass('active');
    })

    $('.next3').click(function(){
        $('.pagination').find('.circle.active').next().addClass('active');
        $('.pagination').find('.circle.active').prev().removeClass('active');
    })

    $('.next4').click(function(){
        $('.pagination').find('.circle.active').next().addClass('active');
        $('.pagination').find('.circle.active').prev().removeClass('active');
    })

    $('.next5').click(function(){
        $('.pagination').find('.circle.active').next().addClass('active');
        $('.pagination').find('.circle.active').prev().removeClass('active');
    })

    $('.prev').click(function(){
        $('.pagination').find('.circle.active').prev().addClass('active');
        $('.pagination').find('.circle.active').next().removeClass('active');
    })

    $('.prev2').click(function(){
        $('.pagination').find('.circle.active').prev().addClass('active');
        $('.pagination').find('.circle.active').next().removeClass('active');
    })

    $('.prev3').click(function(){
        $('.pagination').find('.circle.active').prev().addClass('active');
        $('.pagination').find('.circle.active').next().removeClass('active');
    })

    $('.prev4').click(function(){
        $('.pagination').find('.circle.active').prev().addClass('active');
        $('.pagination').find('.circle.active').next().removeClass('active');
    })

    $('.prev5').click(function(){
        $('.pagination').find('.circle.active').prev().addClass('active');
        $('.pagination').find('.circle.active').next().removeClass('active');
    })
    
})




let hideElements = function() {
   
    let welcomeSection = document.getElementById('welcomeSection')

    let section1 = document.getElementById('section1')

    let section2 = document.getElementById('section2')

    let section3 = document.getElementById('section3')

    let section4 = document.getElementById('section4')

    let section5 = document.getElementById('section5')
     
    let button = document.getElementById('welcomeButton')

    let next = document.querySelector('.next')

    let prev = document.querySelector('.prev')

    let prev2 = document.querySelector('.prev2')

    let prev3 = document.querySelector('.prev3')

    let prev4 = document.querySelector('.prev4')

    let prev5 = document.querySelector('.prev5')

    button.addEventListener('click', function(){
        welcomeSection.style.display = 'none';
        section1.style.display = 'block';
    })

    next.addEventListener('click', function(){
        section1.style.display = 'none';
        section2.style.display = 'block';
    })

    prev.addEventListener('click', function(){
        section1.style.display = 'none';
        welcomeSection.style.display = 'block';
        
    })

    let next2 = document.querySelector('.next2')

    next2.addEventListener('click', function(){
        section2.style.display = 'none';
        section3.style.display = 'block';
    })

    prev2.addEventListener('click', function(){
        section2.style.display = 'none';
        section1.style.display = 'block';
    })

    let next3 = document.querySelector('.next3')

    next3.addEventListener('click', function(){
        section3.style.display = 'none';
        section4.style.display = 'block'
    })

    prev3.addEventListener('click', function(){
        section3.style.display = 'none';
        section2.style.display = 'block'
    })


    let next4 = document.querySelector('.next4')

    next4.addEventListener('click', function(){
        section4.style.display = 'none';
        section5.style.display = 'block';
    })

    prev4.addEventListener('click', function(){
        section4.style.display = 'none';
        section3.style.display = 'block';
    })

    prev5.addEventListener('click', function(){
        section5.style.display = 'none';
        section4.style.display = 'block';
    })
}

hideElements()





// make dropDown


let getSkills = function(){
    fetch('https://bootcamp-2022.devtest.ge/api/skills')
        .then(response => response.json())

        .then(item => item.map(dataItem => {

            let selectEl = document.getElementById('selectElem')

            let option = document.createElement('option')

            let span = document.createElement('p')

            option.textContent = dataItem.title

        
            selectEl.style.width = '100%'

            selectEl.appendChild(option)

            
            let elem = document.querySelector('.dropDown')
            
            elem.appendChild(span)
            elem.appendChild(selectEl)

            elem.style.color = 'black'

            option.addEventListener('click', function(){  

        
                let addElem = document.querySelector('.element-container')

    
                // input.removeAttribute("placeholder")

                h4.classList.add('h4-absolute')
                // addElem.appendChild(h4)

                dropDown.style.display = 'none';
                button.style.display = 'block';
                clickElem.style.display = 'block';
                upDownImg.style.display = 'none';

            })

        
        }))
       
}

getSkills()



let clickElem = document.getElementById('dropDownImg')

let dropDown = document.querySelector('.dropDown')

let button = document.querySelector('.addButton')

let upDownImg = document.getElementById('upDown')

clickElem.addEventListener('click', function(){
    dropDown.style.display = 'block';
    button.style.marginTop = '60px';
    clickElem.style.display = 'none';
    upDownImg.style.display = 'block';
    
})


upDownImg.addEventListener('click', function(){
    upDownImg.style.display = 'none';
    clickElem.style.display = 'block';
    dropDown.style.display = 'none';
    button.style.marginTop = '0px';
})




function myFunction(e) {
    e.preventDefault()
    document.getElementById('forVector').value = e.target.value
}





// input value for button submit



let getValueButton = document.querySelector('.addButton')

getValueButton.addEventListener('click', function(){

    let tokenAmount = document.getElementById('inputValue').value;


    let input = document.getElementById('forVector').value


    var addValue = document.createElement('div')


    if(input != false){

        let valueContainer = document.querySelector('.value-container')

        valueContainer.appendChild(addValue)

    }

    addValue.classList.add('valueStyles')


    addValue.innerHTML = `<span class="forJavaInput">${input}</span> <p>Years of experience:  ${tokenAmount}</p> <div class="removeInput"><div class="horizontal"></div></div>`


    let removeInput = document.querySelector('.removeInput')


    removeInput.addEventListener('click', function(e){


        addValue.remove(e.target)
         
        
    })


})




// choose a date js


let date = document.getElementById('date')

let date2 = document.getElementById('date2')

let hideIMG = document.getElementById('hideIMG')

let hideIMG2 = document.getElementById('hideIMG2')


date.addEventListener('focus', function(){
    hideIMG.style.display = 'none'
})


date2.addEventListener('focus', function(){
    hideIMG2.style.display = 'none'
})



// section 4 input focus



let buttonInput = document.getElementById('getButton')

let focusInput  = document.querySelector('.input')

buttonInput.addEventListener('click', function(e){
    e.preventDefault()
    focusInput.style.display = 'block'
    buttonInput.style.display = 'none'

})






let goBack = document.querySelector('.go-back')

goBack.addEventListener('click', function(){
    section4.style.display = 'block'
})



let section6 = document.getElementById('section6')

let submit = document.querySelector('.submit')


submit.addEventListener('click', function(){
    section5.style.display = 'none'
    section6.style.display = 'block'
})








// let getValue = function() {

//     let tokenAmount = document.getElementById('forVector').value
//     console.log(tokenAmount)

// }   






















// p.forEach(list => {
//     console.log(list)
// })





//     let p = document.querySelectorAll('.forAdd')

//     p.forEach(item => {
//         console.log(item)
//     })
    
// let addElement = function(){

//     p.addEventListener('click', function(){

//         let elem = document.createElement('div')
    
//         elem.appendChild(p)
    
//         elem.classList.add('dropDownStyle')
    
//         let relative = document.querySelector('.relative')
    
//         relative.appendChild(elem)
        
      
//     })
// }

// addElement()
















// let changePage = function(){

//     document.querySelector('.next').onclick = function() {
//         location.href = 'techniucal.html'
//         location.href.find('.circle.active') = 'satesto.html'
//     }


//     document.querySelector('.prev').onclick = function() {
//         location.href = 'personal.html'
//     }
// }
// changePage()